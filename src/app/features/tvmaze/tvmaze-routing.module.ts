import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TvmazeComponent } from './tvmaze.component';

const routes: Routes = [{ path: '', component: TvmazeComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TvmazeRoutingModule { }
