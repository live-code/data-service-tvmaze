import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Series, Show } from './model/series';
import { NgForm } from '@angular/forms';
import { TvmazeService } from './services/tvmaze.service';

@Component({
  selector: 'app-tvmaze',
  template: `
    <app-tvmaze-search (search)="tvmazeService.search($event)"></app-tvmaze-search>
   
    <app-tvmaze-list
      [series]="tvmazeService.series"
      (itemClick)="tvmazeService.itemClickHandler($event)"
    ></app-tvmaze-list>
    
    <app-tvmaze-modal 
      [data]="tvmazeService.seriesDetails"
      (closeModal)="tvmazeService.closeModal()"
    ></app-tvmaze-modal>
  `,

})
export class TvmazeComponent  {
  constructor(
    public tvmazeService: TvmazeService,
    private http: HttpClient
  ) {
    this.tvmazeService.search('soprano')

    http.get('http://localhost:3000/users')
      .subscribe(
        val => console.log(val),
        err => console.log('err', err)
      )
  }

}

