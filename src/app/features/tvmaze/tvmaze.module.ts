import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TvmazeRoutingModule } from './tvmaze-routing.module';
import { TvmazeComponent } from './tvmaze.component';
import { HelloComponent } from '../../shared/components/hello.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { TvmazeListComponent } from './components/tvmaze-list.component';
import { TvmazeSearchComponent } from './components/tvmaze-search.component';
import { TvmazeModalComponent } from './components/tvmaze-modal.component';
import { TvmazeService } from './services/tvmaze.service';


@NgModule({
  declarations: [TvmazeComponent, TvmazeListComponent, TvmazeSearchComponent, TvmazeModalComponent],
  imports: [
    CommonModule,
    TvmazeRoutingModule,
    SharedModule,
    FormsModule
  ],
  providers: [
    TvmazeService
  ]
})
export class TvmazeModule { }
