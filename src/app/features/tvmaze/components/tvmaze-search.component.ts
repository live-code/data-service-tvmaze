import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-tvmaze-search',
  template: `
    <form #f="ngForm" (submit)="search.emit(f.value.text)">
      <input type="text" [ngModel] name="text">
      <button type="submit">SEARCH</button>
    </form>
  `,
})
export class TvmazeSearchComponent {
  @Output() search: EventEmitter<string> = new EventEmitter();
}
