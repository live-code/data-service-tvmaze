import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Show } from '../model/series';

@Component({
  selector: 'app-tvmaze-modal',
  template: `
    <div
      class="wrapper"
      *ngIf="data">

      <div class="content">

        <div class="closeButton" (click)="closeModal.emit()">×</div>
        <h1>{{data.name}}</h1>

        <li class="tag" *ngFor="let genres of data.genres">{{genres}}</li>

        <img *ngIf="data.image" [src]="data.image.original" width="100%">
        <div [innerHTML]="data.summary"></div>
        
        <div *ngIf="!data.summary">Nessuna descrizione</div>
        
        <hr>
        <button class="button" *ngIf="data.url"
                (click)="openWebSite(data.url)">
          Visit website
        </button>
      </div>
    </div>
  `,
  styleUrls: [
    './tvmaze-modal.component.css'
  ]
})
export class TvmazeModalComponent  {
  @Input() data: Show;
  @Output() closeModal: EventEmitter<void> = new EventEmitter()

  openWebSite(url: string) {
    window.open(url)
  }
}
