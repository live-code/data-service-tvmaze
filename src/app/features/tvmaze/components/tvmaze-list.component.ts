import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Series } from '../model/series';

@Component({
  selector: 'app-tvmaze-list',
  template: `
    <div class="grid">
      <div 
        *ngFor="let s of series" class="grid-item"
        (click)="itemClick.emit(s)"
      >
        <div class="movie">
          <img *ngIf="s.show.image" [src]="s.show.image.medium" alt="">
          <div class="noImage" *ngIf="!s.show.image"></div>
          <div class="movie-text">{{s.show.name}}</div>
        </div>
      </div>
    </div>

  `,
  styleUrls: ['./tvmaze-list.component.css',]
})
export class TvmazeListComponent {
  @Input() series: Series[]
  @Output() itemClick: EventEmitter<Series> = new EventEmitter()
}
