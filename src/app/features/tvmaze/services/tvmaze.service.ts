import { Injectable } from '@angular/core';
import { Series, Show } from '../model/series';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class TvmazeService {
  series: Series[];
  seriesDetails: Show;

  constructor(private http: HttpClient) {}

  itemClickHandler(series: Series) {
    this.seriesDetails = series.show;
  }

  closeModal() {
    this.seriesDetails = null;
  }

  search( text: string ) {
    this.http.get<Series[]>(`http://api.tvmaze.com/search/shows?q=${text}`)
      .subscribe(result => {
        this.series = result;
      })
  }
}
