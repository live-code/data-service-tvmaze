import { Component, OnDestroy, OnInit } from '@angular/core';
import { Credentials } from '../../model/credentials';
import { AuthService } from '../../core/auth/auth.service';
import { Router } from '@angular/router';
import { filter, tap } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  template: `
    
    <pre>{{authService.data$}}</pre>
    <form #f="ngForm" (submit)="login(f.value)">
      <input type="text" placeholder="username" ngModel name="username">
      <input type="text" placeholder="pass" ngModel name="password">
      <button type="submit">Send</button>
    </form>
  `,
})
export class LoginComponent implements OnDestroy {
  sub: Subscription;

  constructor(
    public authService: AuthService,
    private router: Router
  ) {

    this.sub = this.authService.isLogged$
      .pipe(
        tap(console.log),
        filter(val => !!val)
      )
      .subscribe(() => {
          this.router.navigateByUrl('tvmaze')
      })
  }

  login(data: Credentials) {
    this.authService.login(data)
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
