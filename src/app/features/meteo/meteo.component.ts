import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { catchError, debounce, debounceTime, distinctUntilChanged, filter, switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { Meteo } from './model/meteo';

@Component({
  selector: 'app-meteo-page',
  template: `
    <input type="text" placeholder="Search" [formControl]="inputText">
    <app-meteo [data]="meteo"></app-meteo>
  `,
})
export class MeteoComponent implements OnInit {
  inputText: FormControl = new FormControl('');
  meteo: Meteo;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.inputText.valueChanges
      .pipe(
        filter(v => v.length > 2),
        debounceTime(1000),
        distinctUntilChanged(),
        switchMap(
          text => this.http.get(`http://api.openweathermap.org/data/2.5/weather?q=${text}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
            .pipe(
              catchError(err => of(null))
            )

        )
      )
      .subscribe(value => {
        this.meteo = value;
      })
  }


  search() {
    console.log(this.inputText.value)
  }
}
