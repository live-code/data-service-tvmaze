import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MeteoRoutingModule } from './meteo-routing.module';
import { MeteoComponent } from './meteo.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [MeteoComponent],
  imports: [
    CommonModule,
    MeteoRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class MeteoModule { }
