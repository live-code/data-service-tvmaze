import { AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { GroupComponent } from '../../shared/components/group.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-uikit2',
  template: `
    <div class="container">
      
      
      <app-list 
        title="COUNTRIES"
        [items]="countries" 
        (iconClick)="doSomething($event)"
      >
        <button (click)="doSomething(123)">Go to</button>
        <button >Move to</button>
      </app-list>
      
      <app-list 
        [title]="'USERS'"
        [items]="users" 
        (iconClick)="doSomethingElse($event)"
      >
        <input type="text" placeholder="search user">
      </app-list>
  
      <app-grid>
        <div>1</div>
        <div>2</div>
        <button class="btn btn-primary">3</button>
        <div>3</div>
      </app-grid>
      
      <app-accordion>
        <app-group title="one">
          <input type="text">
        </app-group>
        
        <app-group title="tw">
          <input type="text">
        </app-group>
        
        <app-group title="three" >
          <button>1</button>
          <button>2</button>
        </app-group>
      </app-accordion>
    </div>

    <form [formGroup]="form">
      <input type="text" placeholder="Search" formControlName="text">
      <select formControlName="unit">
        <option value="metric">C</option>
        <option value="imperial">F</option>
      </select>

      <button [disabled]="form.invalid">SEARCH</button>
    </form>
    
    <app-meteo-http 
      [city]="cityText"
      [unit]="unit"
      title="WEATHER COMPO"
    ></app-meteo-http>
    
    <pre>{{cityText | json}}</pre>
    
  `,
})
export class Uikit2Component  {
  form: FormGroup;
  cityText: string;
  unit: string = 'metric';

  constructor(fb: FormBuilder) {
    this.form = fb.group({
      text: ['', Validators.required],
      unit: ['metric', Validators.required],
    })
    this.form.valueChanges
      .pipe(
        debounceTime(1000)
      )
      .subscribe(data => {
        this.cityText = data.text;
        this.unit = data.unit;
      })
  }


  countries = [
    { id: 1, label: 'Italy', icon: 'fa fa-times'},
    { id: 2, label: 'Germany', icon: 'fa fa-bluetooth'},
    { id: 3, label: 'Spin', icon: 'fa fa-link'},
  ]

  users = [
    { id: 1, label: 'Ciccio', icon: 'fa fa-play'},
    { id: 2, label: 'Mario'},
    { id: 3, label: 'Pippo', icon: 'fa fa-link'},
  ]

  doSomething(country) {
    console.log(country)
  }

  doSomethingElse(user) {
    console.log(user)
  }

}
