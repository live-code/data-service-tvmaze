import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Uikit2Component } from './uikit2.component';

const routes: Routes = [{ path: '', component: Uikit2Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Uikit2RoutingModule { }
