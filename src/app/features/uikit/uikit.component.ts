import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-uikit',
  template: `
    
    <div appRow="md">
      <div appCol="6">L</div>
      <div appCol="3">C</div>
      <div appCol="3">R</div>
    </div>
    
    <div [appPad]="10">Esempio 10 padding</div>
    <button appPad="20">Esempio 20 padding</button>
    <div appColor="red" bg="yellow">Forza Roma</div>
    
    <div 
      appUrl="http://www.fabiobiondi.io"
    >Visit website</div>
    
    <button 
      appUrl="http://www.pippo.com" 
      target="_self"
    >xyz</button>
  `,
})
export class UikitComponent {

}
