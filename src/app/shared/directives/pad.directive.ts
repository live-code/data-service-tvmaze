import { Directive, ElementRef, HostBinding, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appPad]'
})
export class PadDirective {
  @Input() set appPad(value: number) {
    // this.el.nativeElement.style.padding = value + 'px';
    this.renderer.setStyle(this.el.nativeElement, 'padding' , value + 'px');
  }

  constructor(
    private el: ElementRef,
    private renderer: Renderer2
  ) {
    console.log(el.nativeElement)
  }
  /*@HostBinding('style.margin') get margin() {
    console.log('helll')
    return this.appPad + 'px'
  }*/

}
