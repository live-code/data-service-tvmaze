import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appColor]'
})
export class ColorDirective {
  @Input() appColor: string;
  @Input() bg: string;

  @HostBinding('style.color') get color() {
    return this.appColor;
  }

  @HostBinding('style.background-color') get background() {
    return this.bg;
  }

}
