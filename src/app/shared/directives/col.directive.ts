import { Directive, HostBinding, Input, Optional } from '@angular/core';
import { RowDirective } from './row.directive';

@Directive({
  selector: '[appCol]'
})
export class ColDirective {
  @Input() appCol: number | string;

  @HostBinding() get class() {
    return `col-${this.parent?.appRow || 'sm'}-${this.appCol}`;
    // return 'col-' + (this.parent?.appRow || 'sm') + '-' + this.appCol;
  }

  constructor(@Optional() private parent: RowDirective ) {
    console.log(parent)
  }

}
