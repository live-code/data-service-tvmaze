import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[appHello]'
})
export class HelloDirective {
  @HostBinding() innerHTML = 'ciao';
  @HostBinding('style.fontSize') fontSize = '50px'
  @HostBinding('style.margin-top') mTop = '50px'
}

