import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appRow]'
})
export class RowDirective {
  @Input() appRow: string;
  @HostBinding() class = 'row'

  constructor() { }

}
