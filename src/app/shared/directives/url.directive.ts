import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appUrl]'
})
export class UrlDirective {
  @Input() appUrl: string;
  @Input() target: string = '_blank'

  @HostListener('click')
  clickHandler() {
    if (this.target === '_self') {
      location.href = this.appUrl;
    } else {
      window.open(this.appUrl)
    }
  }

  // @HostBinding() class = 'url'
  @HostBinding('style.cursor') cursor = 'pointer'
}
