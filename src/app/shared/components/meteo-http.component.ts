import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Meteo } from '../../features/meteo/model/meteo';

@Component({
  selector: 'app-meteo-http',
  template: `
    <div *ngIf="meteo">
      <h1>{{title}}</h1>
      <h4> {{city}}</h4>
      <pre>{{meteo | json}}</pre>
    </div>
  `,
})
export class MeteoHttpComponent implements OnChanges {
  @Input() city: string;
  @Input() unit: string;
  @Input() title: string;

  meteo: Meteo;

  constructor(private http: HttpClient) { }

  ngOnChanges(changes: SimpleChanges) {
    if ( (changes.city?.currentValue || changes.unit?.currentValue) &&  this.city ) {
      this.http.get(`http://api.openweathermap.org/data/2.5/weather?q=${this.city}&units=${this.unit}&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
        .pipe(
          catchError(err => of(null))
        )
        .subscribe(res => this.meteo = res)
    };

  }
}
