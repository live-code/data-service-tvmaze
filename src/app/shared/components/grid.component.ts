import { AfterContentInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-grid',
  template: `
    <div class="row" #host>
      <ng-content></ng-content>
    </div>
  `,
})
export class GridComponent {
  @ViewChild('host') host: ElementRef<HTMLDivElement>;

  ngAfterViewInit() {
    const el = this.host.nativeElement.children
    for (let i=0; i < el.length; i++) {
      el[i].classList.add('col')
    }
  }

}
