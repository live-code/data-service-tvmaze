import { AfterViewInit, Component, ContentChildren, OnInit, QueryList, ViewChildren } from '@angular/core';
import { GroupComponent } from './group.component';

@Component({
  selector: 'app-accordion',
  template: `
    <div style="border: 2px dashed blue" >
      <ng-content></ng-content>
    </div>
  `,
})
export class AccordionComponent implements AfterViewInit {
  @ContentChildren(GroupComponent) groups: QueryList<GroupComponent>;

  ngAfterViewInit() {
      this.groups.toArray().forEach(g => {
        g.toggle.subscribe(() => {
          this.openGroup(g)
        })
      })
  }

  openGroup(groupToOpen: GroupComponent) {
    this.groups.toArray().forEach(g => {
      g.opened = false;
    })
    groupToOpen.opened = true;
  }

}
