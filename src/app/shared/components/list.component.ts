import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-list',
  template: `
    <ul>
      <li class="list-group-item list-group-item-dark">
        {{title}}
      </li>
      
      <li *ngFor="let item of items" class="list-group-item">
        {{item.label}}
        <div class="pull-right" *ngIf="item.icon">
          <i [class]="item.icon" (click)="iconClick.emit(item)"></i>
        </div>
      </li>

      <li class="list-group-item list-group-item-dark">
        <ng-content></ng-content>
      </li>

    </ul>
  `,
})
export class ListComponent  {
  @Input() title: string;
  @Input() items: any[];
  @Output() iconClick: EventEmitter<any> = new EventEmitter<any>();
}
