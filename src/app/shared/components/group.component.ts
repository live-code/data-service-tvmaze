import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-group',
  template: `
    
    <div class="card">
      <div class="card-header" (click)="toggle.emit()">{{title}}</div>
      <div class="card-body" *ngIf="opened">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class GroupComponent {
  @Input() title: string
  @Input() opened = false;
  @Output() toggle: EventEmitter<void> = new EventEmitter<void>();
}
