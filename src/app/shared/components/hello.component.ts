import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-hello',
  template: `<h1>hello {{name}} !!!!</h1>`
})
export class HelloComponent {
  @Input() name = 'Ciccio';
}
