import { Component, Input, OnInit } from '@angular/core';
import { Meteo } from '../../features/meteo/model/meteo';

@Component({
  selector: 'app-meteo',
  template: `
    <div *ngIf="data">
      <pre>{{data.main.temp}}°</pre>
      <pre>{{data.main.pressure}}</pre>
      <img [src]="'https://openweathermap.org/img/w/' + data.weather[0].icon + '.png'" alt="">
    </div>
  `,
})
export class MeteoComponent  {
  @Input() data: Meteo;
}

