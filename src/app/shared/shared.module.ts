import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HelloComponent } from './components/hello.component';
import { HelloDirective } from './directives/hello.directive';
import { PadDirective } from './directives/pad.directive';
import { ColorDirective } from './directives/color.directive';
import { UrlDirective } from './directives/url.directive';
import { RowDirective } from './directives/row.directive';
import { ColDirective } from './directives/col.directive';
import { ListComponent } from './components/list.component';
import { GridComponent } from './components/grid.component';
import { GroupComponent } from './components/group.component';
import { AccordionComponent } from './components/accordion.component';
import { MeteoComponent } from './components/meteo.component';
import { MeteoHttpComponent } from './components/meteo-http.component';

@NgModule({
  declarations: [
    HelloComponent, HelloDirective, PadDirective, ColorDirective, UrlDirective, RowDirective, ColDirective,
    ListComponent,
    GridComponent,
    GroupComponent,
    AccordionComponent,
    MeteoComponent,
    MeteoHttpComponent
  ],
  exports: [
    HelloComponent, HelloDirective, PadDirective, ColorDirective, UrlDirective, RowDirective, ColDirective,
    ListComponent,
    GridComponent,
    GroupComponent,
    AccordionComponent,
    MeteoComponent,
    MeteoHttpComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
