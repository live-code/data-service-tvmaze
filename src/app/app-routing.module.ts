import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/auth/auth.guard';


const routes: Routes = [
  { path: 'tvmaze', canActivate: [AuthGuard], loadChildren: () => import('./features/tvmaze/tvmaze.module').then(m => m.TvmazeModule) },
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  { path: 'uikit', loadChildren: () => import('./features/uikit/uikit.module').then(m => m.UikitModule) },
  { path: 'uikit2', loadChildren: () => import('./features/uikit2/uikit2.module').then(m => m.Uikit2Module) },
  { path: 'meteo', loadChildren: () => import('./features/meteo/meteo.module').then(m => m.MeteoModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
