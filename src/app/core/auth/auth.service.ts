import { Injectable } from '@angular/core';
import { Credentials } from '../../model/credentials';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Auth } from './auth';
import { filter, map, mergeMap, mergeMapTo } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthService {
  _data$: BehaviorSubject<Auth> = new BehaviorSubject<Auth>(null);
  data$ = this._data$.asObservable();

  constructor(private http: HttpClient) {}

  login(data: Credentials) {
    this.http.get<Auth>('http://localhost:3000/login')
      .subscribe(res => {
        localStorage.setItem('token', res.token);
        localStorage.setItem('role', res.role);
        this._data$.next(res);
      })
  }

  logout() {
    localStorage.removeItem('token')
    localStorage.removeItem('role')
    this._data$.next(null)
  }

  get token$(): Observable<string> {
    return this.data$
      .pipe(
        mergeMap(data => of(localStorage.getItem('token')))
      )
  }

  get role$(): Observable<string> {
    return this.data$
      .pipe(
        mergeMap(data => of(localStorage.getItem('role')))
      )
  }


  get isLogged$(): Observable<boolean> {
    return this.data$
      .pipe(
        mergeMap(() => of(!!localStorage.getItem('token')))
      )
  }

  get isLogged(): boolean {
    return !!localStorage.getItem('token');
  }

  /*
    get token$(): Observable<string> {
    return this.data$
      .pipe(
        map(data => data?.token)
      )
  }


  get isLogged$(): Observable<boolean> {
    return this.data$
      .pipe(
        map(data => !!data),
      )
  }*/
}
