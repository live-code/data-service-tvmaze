import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { iif, Observable, of, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { catchError, first, mergeMap, mergeMapTo, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService,
    private router: Router
  ) {  }

  // VERSION 4: use 3 observables although we don't need all of them
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.authService.isLogged$
      .pipe(
        mergeMap(() => this.authService.token$),
        first(),
        mergeMap(token => iif(
          () => token && req.url.includes('localhost:3000'),
          next.handle(req.clone({ setHeaders: { 'authorization': 'Bearer ' + token } })),
          next.handle(req)
        )),
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch (err.status) {
              case 401:
                console.log('token scaduto')
                // Toast.notification('errore')
                break;

              case 400:
              case 404:
                break;

              default:
              // case 404:
                this.authService.logout();
                this.router.navigateByUrl('login')
                break;
            }
          }
          // return of(err)
          return throwError(err)
        })
      )
  }


  // VERSION 3: OK!! SUPER! IDIOMATIC
  intercept3(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.authService.token$
      .pipe(
        first(),
        mergeMap(token => iif(
          () => token && req.url.includes('localhost:3000'),
          next.handle(req.clone({ setHeaders: { 'authorization': 'Bearer ' + token } })),
          next.handle(req)
        ))
      )
  }




  // VERSIONE 2: idiomatic way
  intercept2(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.authService.token$
      .pipe(
        first(),
        mergeMap(token => {
          let cloned = req;
          if (token && req.url.includes('localhost:3000')) {
            cloned = req.clone({ setHeaders: { 'authorization': 'Bearer ' + token } })
          }
          return next.handle(cloned)
        })
      )
  }



  // VERSIONE 1: getValue => bad practice!!
  intercept1(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let cloned = req;
    const tk = this.authService._data$.getValue()?.token;

    if (!!tk && req.url.includes('localhost:3000')) {
      cloned = req.clone({
        setHeaders: { 'authorization': 'Bearer ' + tk }
      })
    }
    return next.handle(cloned)
      /*
     .pipe(
        catchError()
        // da completare. .. vedi sopra
      )
      */
  }

}
