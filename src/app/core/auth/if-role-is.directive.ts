import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from './auth.service';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[appIfRoleIs]'
})
export class IfRoleIsDirective {
  @Input() appIfRoleIs: string;
  sub: Subscription;

  constructor(
    private authService: AuthService,
    private view: ViewContainerRef,
    private template: TemplateRef<any>
  ) {
    this.sub = this.authService.role$
      .subscribe(role => {
        if (role === this.appIfRoleIs) {
          view.createEmbeddedView(template)
        } else {
          view.clear();
        }
       console.log(role)
      })
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }


}
