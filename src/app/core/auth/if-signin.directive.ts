import { Directive, ElementRef, OnDestroy, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from './auth.service';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[appIfSignin]'
})
export class IfSigninDirective implements OnDestroy {
  sub: Subscription;

  constructor(
    private authService: AuthService,
    private view: ViewContainerRef,
    private template: TemplateRef<any>
  ) {
    this.sub = this.authService.isLogged$
      .subscribe(value => {
        if (value) {
          view.createEmbeddedView(template)
        } else {
          view.clear();
        }
      })
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}
