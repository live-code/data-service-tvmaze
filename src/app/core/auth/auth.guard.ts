import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { filter, map, switchMap, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private http: HttpClient, private router: Router) {}

  canActivate(): Observable<boolean> {
    /*return this.http.get<{ tempToken: string }>('http://localhost:3000/validateToken')
      .pipe(
        switchMap(result => this.http.get<{ response: string}>('http://localhost:3000/checkPlan?temp=' + result.tempToken)),
        map(result => result.response === 'ok'),
        tap(valid => {
          if (!valid) {
            this.router.navigateByUrl('login')
          }
        })
      )*/

    return this.authService.isLogged$
      .pipe(
        tap(val => {
          if (!val) {
            this.router.navigateByUrl('login')
          }
        })
      )
  }

}
