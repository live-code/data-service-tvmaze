import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar.component';
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './auth/auth.interceptor';
import { SharedModule } from '../shared/shared.module';
import { IfRoleIsDirective } from './auth/if-role-is.directive';
import { IfSigninDirective } from './auth/if-signin.directive';
import { IfLoggedDirective } from './auth/if-logged.directive';

@NgModule({
  declarations: [
    NavbarComponent,
    IfLoggedDirective, IfSigninDirective, IfRoleIsDirective,
  ],
  exports: [
    NavbarComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ]
})
export class CoreModule { }
