import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Auth } from '../auth/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  template: `
    <button routerLink="login" *ngIf="!(authService.isLogged$ | async)">login</button>
    <button [routerLink]="'tvmaze'" routerLinkActive="bg" appIfLogged>tvmaze</button>
    <button [routerLink]="'uikit'" routerLinkActive="bg" *appIfRoleIs="'admin'">Uikit</button>
    <button [routerLink]="'uikit2'" routerLinkActive="bg">Uikit2</button>
    <button [routerLink]="'meteo'" routerLinkActive="bg">meteo</button>
    <button (click)="logoutHandler()">Logout</button>
    {{(authService.data$ | async)?.displayName}}
  `,
  styles: [`
    .bg { background-color: red}  
  `]
})
export class NavbarComponent {
  constructor(
    public authService: AuthService,
    private router: Router
  ) {}

  logoutHandler() {
    this.authService.logout();
    this.router.navigateByUrl('login')
  }
}
